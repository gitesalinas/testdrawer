var ToolFactory = (function($, Motion, ShapeFactory, SVG) {

    function _() {
        var _id = new Date().getTime();
        var _name = arguments[0] + "_" + _id;

        this.getId = function() { return _id; };
        this.getName = function() { return _name; };
        this.setName = function(name) { _name = name; }
    };

    function Canvas(width, height) {

        _.call(this, "canvas");

        this.width = width || 600;
        this.height = height || 630;

        this.objects = {};

        this.canvas = undefined;

        this.$ = undefined;

        this.draw = function() {
            this.canvas = SVG("wrapper-esaw-drawer-canvas");
            this.canvas.size(this.width, this.height);
            this.canvas.attr({ id: this.getId(), "class": "esaw-drawer-canvas" })
            
            this.$ = $("svg#" + this.getId());
        };

        this.clearObjects = function() {
            for(var i = 0; i < this.objects.length; i++) {
                //this.objects[i].dispose();
                this.canvas.removeChild(objects[i]);
            }
        };

        this.addObject = function(object) {
            //this.l.appendChild(object.l);
            this.objects[object.getId()] = object;
        };

        this.removeObject = function(object) {
            delete this.objects[object.getId()];
            object.dispose();
        };
    };
    

    function Drawer(canvas) {
        
        var CANVAS_PADDING = 0;
        var OBJECT_PADDING_FIX = 0;

        _.call(this, "drawer");

        this.canvas = canvas;

        this.changeCanvas = function(canvas) {
            this.canvas = canvas;
        };

        this.getCanvas = function() {
            return this.canvas;
        };

        this.drawRectangle = function() {
            var object = ShapeFactory.getRectangle(this.canvas.canvas, {x: 0, y: 0}, 100, 100);
            this.canvas.addObject(object);
            Motion.bindEvents(object);

            enableDraggable(object, canvas);
        };

        this.drawCircle = function() {
            var object = ShapeFactory.getCircle(this.canvas.canvas, {x: 0, y: 0}, 100);
            this.canvas.addObject(object);
            Motion.bindEvents(object);

            enableDraggable(object, canvas);
        };

        this.drawTriangle = function() {
            var object = ShapeFactory.getTriangle(this.canvas.canvas, {x: 0, y: 0}, 100, 100);
            this.canvas.addObject(object);
            Motion.bindEvents(object);

            enableDraggable(object, canvas);
        };

        this.drawStar = function() {
            var object = ShapeFactory.getStar(this.canvas.canvas, {x: 0, y: 0}, 100, 100);
            this.canvas.addObject(object);
            Motion.bindEvents(object);

            enableDraggable(object, canvas);
        };

        function enableDraggable(object, canvas) {
            var top = CANVAS_PADDING + canvas.$.offset().top;
            var bottom =  canvas.$.offset().top + canvas.height - (object.height - CANVAS_PADDING - OBJECT_PADDING_FIX);
            var left = CANVAS_PADDING + canvas.$.offset().left;
            var right = canvas.$.offset().left + canvas.width - (object.width - CANVAS_PADDING - OBJECT_PADDING_FIX);

            //object.$.draggable({containment : [left, top, right, bottom]});
            object.$.draggable({containment : [left, top, right, bottom]})
                /*.on('mousedown', function(event, ui){
                    // bring target to front
                    $(event.target.parentElement).append( event.target );
                })*/
                .on('drag', function(event, ui){
                    // update coordinates manually, since top/left style props don't work on SVG
                    event.target.setAttribute('transform', "translate(" + (ui.offset.left - left) + ", " + (ui.offset.top - top) + ")");
                    //event.target.setAttribute('y', ui.offset.top - top);
                });
        }

    };

    return {
        createCanvas: function(width, height) {
            Canvas.prototype = new _;
            Canvas.prototype.constructor = Canvas;

            return new Canvas(width, height);
        },
        createDrawer: function(canvas) {
            Drawer.prototype = new _;
            Drawer.prototype.constructor = Drawer;

            return new Drawer(canvas);
        }
    };

})(jQuery, Motion, ShapeFactory, SVG);