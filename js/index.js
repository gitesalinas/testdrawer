
var Context = (function($) {

    var drawer, canvas;

    return {
        init: function() {
            canvas = ToolFactory.createCanvas();
            drawer = ToolFactory.createDrawer(canvas);

            canvas.draw();

            Subscribers.subscribe("removeObject", canvas.removeObject, [canvas]);
        },

        draw: function(method) {
            if(drawer[method]) drawer[method]();
        }
    };


})(jQuery);

window.onload = function() { 
    Context.init();
};
//$(function() {  });




function _(url, successMessenger, errorMessenger, loader, extra) {
    this.url = url || "";
    //this.error = undefined;
    //this.content = undefined;

    this.ajax = $.ajax;

    this.ajaxAware = ESAWAjaxAware.getInstance();

    this.successMessenger = successMessenger;

    this.errorMessenger = errorMessenger;

    this.loader = loader || new function() {
        this.show = function() {  },
        this.hide = function() {  }
    };

    this.formStatusCommunication = new FormStatusCommunication();

    this.formStatusResponse = new FormStatusResponse();

    this.errorHandler = new FormErrorHandler(errorMessenger);

    this.extra = extra || {
        onSuccess: function() {  },
        onError: function() {  },
        onComplete: function() {  }
    };

    this.fail = function(jqXHR, textStatus, errorThrown) {
        this.formStatusCommunication.receiving();
        this.formStatusResponse.failed();
        
        if(this.extra) this.extra.onError.call(this, jqXHR, textStatus, errorThrown);
        
        this.errorHandler.handle(jqXHR, textStatus, errorThrown);
    };

    this.always = function(data, textStatus, jqXHR) {
        this.formStatusCommunication.waiting();

        if(this.extra) this.extra.onComplete.call(this, data, textStatus, jqXHR);

        this.ajaxAware.unsubscribe($caller.attr('id') || $caller.attr('class'));
    };
};


function FormErrorHandler(messenger) {
    
    var ERRORS_CODE = {
        _: 555,
        TOKEN_EXPIRE: 556
    };

    this.messenger = messenger;

    this.handle = function(jqXHR, textStatus, errorThrown) {

        if (errorThrown === 'abort' || jqXHR.status === 0) return;

        if (jqXHR.responseJSON.message) {
            messenger ? messenger.showMessage(jqXHR.responseJSON.message) : console.log(jqXHR.responseJSON.message);
        } else { 
            console.log(textStatus); 
        }

        switch(jqXHR.status) {
            case ERRORS_CODE._: 
                setTimeout(function() { if(jqXHR.responseJSON.redirectTo) location.href = jqXHR.responseJSON.redirectTo; }, 2000); break;
            case ERRORS_CODE.TOKEN_EXPIRE: 
                setTimeout(function() { location.reload(); }, 2000); break;
            default: break;
        }
    };
};


function FormStatusCommunication() {
    var STATUS_CODE = {
        WAITING: 0,
        SENDING: 1,
        RECEIVING: 2
    };

    this.status = STATUS_CODE.WAITING;

    this.waiting = function() { this.status = STATUS_CODE.WAITING; }
    this.receiving = function() { this.status = STATUS_CODE.RECEIVING; }
    this.sending = function() { this.status = STATUS_CODE.SENDING; }

    this.isWaiting = function() {
        return this.status == STATUS_CODE.WAITING;
    };
};


function FormStatusResponse() {
    var STATUS = {
        PENDING: 0,
        DONE: 1,
        FAILED: 2
    };
    
    var state = STATUS.PENDING;
    
    this.done = function() { state = STATUS.DONE; };
    this.failed = function() { state = STATUS.FAILED; };
    
    this.isSuccess = function() { return (state == STATUS.DONE); };
}


function PostForm(url, $form, serialize, successMessenger, errorMessenger, loader, extra) {
    _.call(this, url, successMessenger, errorMessenger, loader, extra)

    this.$form = function () { return $form instanceof jQuery ? $form : $($form); }();
    
    this.sr = serialize || function() {
        return this.$form.serialize();
    };

    this.post = function($caller) {

        if(!this.formStatusCommunication.isWaiting()) return;

        this.formStatusCommunication.sending();

        this.ajaxAware.subscribe($caller.attr('id') || $caller.attr('class'), this.loader);

        this
        .ajax({ url: this.url, type: "POST", data: this.sr() })
        .done(function(data, textStatus, jqXHR) { done.call(this, data, textStatus, jqXHR) }.bind(this))
        .fail(function(jqXHR, textStatus, errorThrown) { this.fail.call(this, jqXHR, textStatus, errorThrown) }.bind(this))
        .always(function(data, textStatus, jqXHR) { this.always.call(this, data, textStatus, jqXHR) }.bind(this));

        function done(data, textStatus, jqXHR) {
            this.formStatusCommunication.receiving();
            this.formStatusResponse.done();

            try {
                this.successMessenger ? this.successMessenger.showMessage(data.message) : console.log(data.message);
				
				if(this.extra) this.extra.onSuccess.call(this, data, textStatus, jqXHR);
				
			} catch(e) { this.errorMessenger ? this.errorMessenger.showMessage("Error procesando respuesta") : console.log(data.message); }
        };
    };
};

PostForm.prototype = new _;
//PostForm.prototype.constructor = PostForm;

function GetHtmlForm(url, $container, render, successMessenger, errorMessenger, loader, extra) {
    _.call(this, url, successMessenger, errorMessenger, loader, extra)

    this.$container = function () { return $container instanceof jQuery ? $container : $($container); }();
    
    this.render = render || function(html) {
        this.$container.html(html);
    };

    this.get = function($caller) {

        if(!this.formStatusCommunication.isWaiting()) return;

        this.formStatusCommunication.sending();

        this.ajaxAware.subscribe($caller.attr('id') || $caller.attr('class'), this.loader);

        this
        .ajax({ url: this.url, type: "GET", data: this.sr() })
        .done(function(data, textStatus, jqXHR) { done.call(this, data, textStatus, jqXHR) }.bind(this))
        .fail(function(jqXHR, textStatus, errorThrown) { this.fail.call(this, jqXHR, textStatus, errorThrown) }.bind(this))
        .always(function(data, textStatus, jqXHR) { this.always.call(this, data, textStatus, jqXHR) }.bind(this));

        function done(data, textStatus, jqXHR) {
            this.formStatusCommunication.receiving();
            this.formStatusResponse.done();

            try {
				this.render(data.html);

				if(this.extra) this.extra.onSuccess.call(this, data, textStatus, jqXHR);
				
			} catch(e) { this.errorMessenger ? this.errorMessenger.showMessage("Error procesando respuesta") : console.log(data.message); }
        };
    };
};

GetHtmlForm.prototype = new _;

function GetForm(url, $element, bindModel, successMessenger, errorMessenger, loader, extra) {
    _.call(this, url, successMessenger, errorMessenger, loader, extra)

    this.$element = function () { return $element instanceof jQuery ? $element : $($element); }();
    
    this.bindModel = bindModel || function(data) {
        
    };

    this.get = function($caller) {

        if(!this.formStatusCommunication.isWaiting()) return;

        this.formStatusCommunication.sending();

        this.ajaxAware.subscribe($caller.attr('id') || $caller.attr('class'), this.loader);

        this
        .ajax({ url: this.url, type: "GET", data: this.sr() })
        .done(function(data, textStatus, jqXHR) { done.call(this, data, textStatus, jqXHR) }.bind(this))
        .fail(function(jqXHR, textStatus, errorThrown) { this.fail.call(this, jqXHR, textStatus, errorThrown) }.bind(this))
        .always(function(data, textStatus, jqXHR) { this.always.call(this, data, textStatus, jqXHR) }.bind(this));

        function done(data, textStatus, jqXHR) {
            this.formStatusCommunication.receiving();
            this.formStatusResponse.done();

            try {
				this.bindModel(data.model);

				if(this.extra) this.extra.onSuccess.call(this, data, textStatus, jqXHR);
				
			} catch(e) { this.errorMessenger ? this.errorMessenger.showMessage("Error procesando respuesta") : console.log(data.message); }
        };
    };
};

GetForm.prototype = new _;

function DeleteForm(url, $form, data, successMessenger, errorMessenger, loader, extra) {
    _.call(this, url, successMessenger, errorMessenger, loader, extra)

    this.$form = function () { return $form instanceof jQuery ? $form : $($form); }();
    
    this.data = data || {};

    this.delete = function($caller) {

        if(!this.formStatusCommunication.isWaiting()) return;

        this.formStatusCommunication.sending();

        this.ajaxAware.subscribe($caller.attr('id') || $caller.attr('class'), this.loader);

        this
        .ajax({ url: this.url, type: "DELETE", data: this.data })
        .done(function(data, textStatus, jqXHR) { done.call(this, data, textStatus, jqXHR) }.bind(this))
        .fail(function(jqXHR, textStatus, errorThrown) { this.fail.call(this, jqXHR, textStatus, errorThrown) }.bind(this))
        .always(function(data, textStatus, jqXHR) { this.always.call(this, data, textStatus, jqXHR) }.bind(this));

        function done(data, textStatus, jqXHR) {
            this.formStatusCommunication.receiving();
            this.formStatusResponse.done();

            try {
				this.successMessenger ? this.successMessenger.showMessage(data.message) : console.log(data.message);
				
				if(this.extra) this.extra.onSuccess.call(this, data, textStatus, jqXHR);

			} catch(e) { this.errorMessenger ? this.errorMessenger.showMessage("Error procesando respuesta") : console.log(data.message); }
        };
    };
};

DeleteForm.prototype = new _;

var ESAWAjaxAware = (function () {

    var _instance = undefined;

    function _() {
        var subjects = {};

        function subscribe(subject, loader) {

            if (subjects[subject]) throw "An ajax call is currently active"

            if (loader) loader.show();

            subjects[subject] = { subject: subject, loader: loader };
        }

        function unsubscribe(subject) {
            if (!subject || !subjects[subject]) return;

            if (subjects[subject].loader) subjects[subject].loader.hide();

            delete subjects[subject];
        }

        return {
            subscribe: subscribe,
            unsubscribe: unsubscribe,
            count: function() { return subjects; }
        };
    };

    return {
        getInstance: function() {
            if(!_instance) {
                
                _instance = new _();
                
                console.log("ESAWAjaxAware instance created");
            }

            return _instance;
        }
    };
})();