var Motion = (function($, Subscribers) {

    function bindHover (object) {
        object.$.mouseover(function() {
            console.log("Hover Element: " + object.getId());
        });
    };

    function bindDblclick(object) {
        object.$.dblclick(function() {
            Subscribers.notify('removeObject', [object]);
        });
    };

    return {
        bindEvents: function(object) {
            bindHover(object);
            bindDblclick(object);
        }
    };

})(jQuery, Subscribers);