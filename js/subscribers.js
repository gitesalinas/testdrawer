
var Subscribers = (function($) {

    var listener = {};

    return {
        subscribe: function(event, callback, arguments) {
            if(!listener[event]) listener[event] = [];
            listener[event].push({ callback: callback, arguments:  arguments});
        },

        unsubscribe: function() {
            //listener[event]
        },

        notify: function(event, arguments) {
            for(var i = 0; i < listener[event].length; i++) {
                $.extend(arguments, listener[event][i].arguments.slice(1));
                listener[event][i].callback.apply(listener[event][i].arguments[0], arguments);
            }
        }
    };


})(jQuery);