var ShapeFactory = (function($, SVG) {

    function _() {

        var _id = new Date().getTime();
        var _name = "shape_" + _id;

        this.getId = function() { return _id; };
        this.getName = function() { return _name; };
        this.setName = function(name) { _name = name; }
    };

    function Shape(position, width, height) {
        this.position = position || {
            x: undefined,
            y: undefined
        }

        this.width = width || 100;
        this.height = height || 100;
    };

    function Rectangle(canvas, position, width, height) {
        Shape.call(this, position, width, height);

        this.l = canvas.rect(this.width, this.height).stroke({ width: 0.5, color: "#b3b3b3" });;

        this.l.attr({ 
            id: this.getId(), 
            "class": "esaw-drawer-shape esaw-drawer-rectangle", 
            fill: "transparent", 
            "stroke-width": "1", 
            "stroke-dasharray": "5",
            "stroke-linecap": "round" });
        
        this.$ = $("rect#" + this.getId());

        this.dispose = function() {
            this.l.remove();
        };
    };

    function Circle(canvas, position, radius) {
        Shape.call(this, position);

        this.radius = radius;

        this.l = canvas.circle(this.radius).stroke({ width: 0.5, color: "#b3b3b3" });;
        
        this.l.attr({ 
            id: this.getId(), 
            "class": "esaw-drawer-shape esaw-drawer-cirle",
            fill: "transparent", 
            "stroke-width": "1", 
            "stroke-dasharray": "5",
            "stroke-linecap": "round" });
        
        this.$ = $("circle#" + this.getId());
        
        this.dispose = function() {
            this.l.remove();
        };
    };

    function Triangle(canvas, position, width) {
        Shape.call(this, position);

        this.l = canvas.polygon('0,100 50,0 100,100').stroke({ width: 0.5, color: "#b3b3b3" });
        
        this.l.attr({ 
            id: this.getId(), 
            "class": "esaw-drawer-shape esaw-drawer-triangle",
            fill: "transparent", 
            "stroke-width": "1", 
            "stroke-dasharray": "5",
            "stroke-linecap": "round" });
        
        this.$ = $("polygon#" + this.getId());

        this.dispose = function() {
            this.l.remove();
        };
    };

    function Star(canvas, position, width) {
        Shape.call(this, position);

        this.l = canvas.polygon('50,0 60,40 100,50 60,60 50,100 40,60 0,50 40,40').stroke({ width: 0.5, color: "#b3b3b3" });
        
        this.l.attr({ 
            id: this.getId(), 
            "class": "esaw-drawer-shape esaw-drawer-star",
            fill: "transparent", 
            "stroke-width": "1", 
            "stroke-dasharray": "5",
            "stroke-linecap": "round" });
        
        this.$ = $("polygon#" + this.getId());

        this.dispose = function() {
            this.l.remove();
        };
    };

    return {
        getRectangle: function(position, width, height) { 
            Shape.prototype = new _;
            Shape.prototype.constructor = Shape;
            Rectangle.prototype = new Shape;
            Rectangle.prototype.constructor = Rectangle;

            var object = new Rectangle(position, width, height);

            return object; 
        },

        getCircle: function(position, width, height) { 
            Shape.prototype = new _;
            Shape.prototype.constructor = Shape;
            Circle.prototype = new Shape;
            Circle.prototype.constructor = Circle;

            var object = new Circle(position, width, height);

            return object; 
        },

        getTriangle: function(position, width, height) { 
            Shape.prototype = new _;
            Shape.prototype.constructor = Shape;
            Triangle.prototype = new Shape;
            Triangle.prototype.constructor = Triangle;

            var object = new Triangle(position, width, height);

            return object; 
        },

        getStar: function(position, width) {
            Shape.prototype = new _;
            Shape.prototype.constructor = Shape;
            Star.prototype = new Shape;
            Star.prototype.constructor = Triangle;

            var object = new Star(position, width);

            return object; 
        }
    };

})(jQuery, SVG);